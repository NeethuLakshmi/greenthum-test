package com.example.greenthumb_test

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

open class BaseActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListner {
    private  var mSnackbar:Snackbar?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }
    private fun showMessage(isConnected: Boolean){
        if (!isConnected){
            val messageToUser = "YOUARE OFFLINE NOW"
        mSnackbar = Snackbar.make(findViewById(R.id.rootLayout),messageToUser,Snackbar.LENGTH_LONG)
            mSnackbar?.duration= Snackbar.LENGTH_INDEFINITE
            mSnackbar?.show()
        }
        else
        {
            mSnackbar?.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListner= this
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showMessage(isConnected)
    }
}