package com.example.greenthumb_test

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
@SuppressLint("Registered")
class ConnectivityReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
       if (connectivityReceiverListner!= null){
               connectivityReceiverListner!!.onNetworkConnectionChanged(isConnectedorConnecting(context))
           }
    }
    private fun isConnectedorConnecting(context: Context?):Boolean {
        var conMgr = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = conMgr.activeNetworkInfo
        return networkInfo!=null && networkInfo.isConnectedOrConnecting

    }
    interface ConnectivityReceiverListner {
        fun onNetworkConnectionChanged(isConnected :Boolean)
    }
    companion object{
        var connectivityReceiverListner :ConnectivityReceiverListner?= null
    }
}